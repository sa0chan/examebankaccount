package com.company;

public class Account {
    protected MonetaryAmount balance;
    protected String ownerName ;

    //initialement le compte bancaire est vide
    public Account(String ownerName, String currency){
        this.ownerName = ownerName;
        balance = new MonetaryAmount(0,currency);
    }

    //recupére le montant actuel du compte.
    public MonetaryAmount getCurrentBalance(){

        System.out.println(ownerName + " a " + balance.getAmount()+ " " + balance.currency);
        return balance;
    }

    //dépot d'argent sur le compte
    public void deposit(double amount){
        balance.addAmount(amount);
    }

    //retirer de l'argent sur le compte . pas possible d'e retirer plus que disponible. dans ce cas il ne se passe rien
    public void withDraw(double amount){
        if(balance.getAmount() > amount){
        balance.substractAmount(amount);
        }else{
            System.out.println("Vous n'avez pas assez d'argent sur votre compte pour retirer cette somme");
        }
    }





}
